<?php

namespace WL\Tests;

use WL\Models\Model;

class MockModel extends Model
{
    public $table = 'mock_models';

    protected static $Rules = [
        'name' => 'required|min:2',
        'email' => 'required|email',
        'password' => 'required|min:6'
    ];
}
