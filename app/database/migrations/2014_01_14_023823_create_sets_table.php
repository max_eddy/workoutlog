<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sets', function (BluePrint $table) {
            $table->increments('id');
            $table->integer('exercise_id')->unsigned();
            $table->integer('session_id')->unsigned();
            $table->integer('reps');
            $table->double('weight');
            $table->timestamps();
            $table->foreign('exercise_id')
                ->references('id')->on('exercises')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('session_id')
                ->references('id')->on('sessions')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sets', function (BluePrint $table) {
            $table->dropForeign('sets_exercise_id_foreign');
            $table->dropForeign('sets_session_id_foreign');
        });

        Schema::drop('sets');
    }
}
