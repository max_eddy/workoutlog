<?php
namespace WL\Repositories;

use WL\Models\User;

class EloquentUserRepository implements UserRepositoryInterface
{
    private $User;

    private $Errors;

    public function __construct(User $user)
    {
        $this->User = $user;
    }

    public function getErrors()
    {
        return $this->Errors;
    }

    /**
     * Create a new user model.
     *
     * @param array $data
     * @return \WL\Models\User
     */
    public function create(array $data)
    {
        if (!$this->User->validate($data)) {
            $this->Errors = $this->User->getErrors();
            return false;
        }

        return User::create($data);
    }

    /**
     * Update the user model with the following data.
     *
     * @param int \WL\Models\User $user
     * @param array $data
     * @return bool
     */
    public function update($id, array $data)
    {
        $user = $this->User->findOrfail($id);
        if (!$user->validate($data)) {
            $this->Errors = $user->getErrors();
            return false;
        }

        // Update password if new password is present, otherwise keep old password.
        if (!empty($data['new_password'])) {
            $data['password'] = $data['new_password'];
        } else {
            unset($data['password']);
        }

        $user->fill($data);
        return $user->save();
    }
}
