<?php
namespace WL\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\View\Environment;
use WL\Repositories\WorkoutRepositoryInterface;

class WorkoutController extends BaseController
{
    private $Input;

    private $Request;

    private $View;

    private $WorkoutRepo;

    protected $layout = 'layout';

    public function __construct(
        Request $request,
        Environment $view,
        WorkoutRepositoryInterface $workoutRepo
    ) {
        $this->Request = $this->Input = $request;
        $this->View = $view;
        $this->WorkoutRepo = $workoutRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if ($this->Request->ajax()) {
            return $this->WorkoutRepo->all();
        }
        $this->layout->content = $this->View->make('workout.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        if ($this->WorkoutRepo->create($this->Input->all())) {
            return;
        }

        return Response::json(['messages' => $this->WorkoutRepo->getErrors()], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return $this->WorkoutRepo->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        if ($this->WorkoutRepo->update($id, $this->Input->all())) {
            return;
        }

        return Response::json(['messages' => $this->WorkoutRepo->getErrors()], 400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return $this->WorkoutRepo->delete($id);
    }
}
