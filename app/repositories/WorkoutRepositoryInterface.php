<?php
namespace WL\Repositories;

use WL\Models\Workout;

interface WorkoutRepositoryInterface
{
    public function getErrors();
    public function all();
    public function create(array $data);
    public function find($id);
    public function update($id, array $data);
    public function delete($id);
}
