<?php

use WL\Models\Exercise;
use WL\Models\User;
use WL\Models\Workout;

class TestWorkoutDataSeeder extends Seeder
{
    public function run()
    {
        $user = User::where('email', 'maxiscool@gmail.com')->first();
        if ($user) {
            $user->delete();
        }
        $user = User::create([
            'email' => 'maxiscool@gmail.com',
            'password' => '1234567',
            'password_confirmation' => '1234567',
            'first_name' => 'Max',
            'last_name' => 'Eddy'
        ]);

        $push          = Workout::create(['user_id' => $user->id, 'name' => 'Push']);
        $benchPress    = Exercise::create(['workout_id' => $push->id, 'name' => 'Bench Press', 'num_sets' => 3]);
        $shoulderPress = Exercise::create(['workout_id' => $push->id, 'name' => 'Shoulder Press', 'num_sets' => 3]);
        $tricepExt     = Exercise::create(['workout_id' => $push->id, 'name' => 'Tricep Extension', 'num_sets' => 3]);

        $pull          = Workout::create(['user_id' => $user->id, 'name' => 'Pull']);
        $pullup        = Exercise::create(['workout_id' => $pull->id, 'name' => 'Pull Up', 'num_sets' => 3]);
        $row           = Exercise::create(['workout_id' => $pull->id, 'name' => 'Dumbbell Row', 'num_sets' => 3]);
        $curl          = Exercise::create(['workout_id' => $pull->id, 'name' => 'Dumbbell Row', 'num_sets' => 3]);

        $legs          = Workout::create(['user_id' => $user->id, 'name' => 'Legs']);
        $squat         = Exercise::create(['workout_id' => $legs->id, 'name' => 'Squat', 'num_sets' => 3]);
        $legCurl       = Exercise::create(['workout_id' => $legs->id, 'name' => 'Leg Curl', 'num_sets' => 3]);
        $calfRaise     = Exercise::create(['workout_id' => $legs->id, 'name' => 'Calf Raise', 'num_sets' => 3]);
    }
}
