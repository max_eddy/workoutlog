<?php
namespace WL\Repositories;

use WL\Models\User;

interface UserRepositoryInterface
{
    public function getErrors();
    public function create(array $data);
    public function update($id, array $data);
}
