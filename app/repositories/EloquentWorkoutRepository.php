<?php
namespace WL\Repositories;

use WL\Models\Workout;

class EloquentWorkoutRepository implements WorkoutRepositoryInterface
{
    private $Workout;

    private $Errors;

    public function __construct(Workout $workout)
    {
        $this->Workout = $workout;
    }

    public function getErrors()
    {
        return $this->Errors;
    }

    public function all()
    {
        return $this->Workout->all();
    }

    public function create(array $data)
    {
        if (!$this->Workout->validate($data)) {
            $this->Errors = $this->Workout->getErrors();
            return false;
        }

        return Workout::create($data);
    }

    public function find($id)
    {
        return $this->Workout->findOrFail($id);
    }

    public function update($id, array $data)
    {
        $workout = $this->find($id);
        if (!$workout->validate($data)) {
            $this->Errors = $workout->getErrors();
            return false;
        }

        $workout->fill($data);
        return $workout->save();
    }

    public function delete($id)
    {
        $workout = $this->find($id);
        return $workout->delete();
    }
}
