<?php
namespace WL\Models;

class Workout extends Model
{
    protected $fillable = ['name'];

    protected static $Rules = [
        'name' => 'required|adsu'
    ];

    public function sessions()
    {
        return $this->hasMany('WL\Models\Session');
    }

    public function exercises()
    {
        return $this->hasMany('WL\Models\Exercise');
    }
}
