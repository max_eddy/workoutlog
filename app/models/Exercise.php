<?php
namespace WL\Models;

class Exercise extends Model
{
    protected $fillable = ['name', 'num_sets'];

    protected static $Rules = [
        'name' => 'required|adsu',
        'num_sets' => 'required|between:1,20'
    ];
}
