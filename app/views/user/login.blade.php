@if ($loginFailed)
    <p class="alert alert-danger">Username or password was incorrect.</p>
@endif
@if ($name)
    <div class="alert alert-info clearfix">
        <p class="pull-left">You are logged in as <a href="{{ route('showProfile') }}">{{ $name }}</a>.</p>
        {{ Form::open(['route' => 'processLogout', 'method' => 'post', 'role' => 'form', 'id' => 'logout', 'class' => 'pull-right']) }}
            {{ Form::submit('Log Out', ['class' => 'btn btn-default btn-primary']) }}
        {{ Form::close() }}
    </div>
@else
    {{ Form::open(['route' => 'processLogin', 'method' => 'post', 'role' => 'form', 'id' => 'login', 'class' => 'clearfix']) }}
        <div class="form-group">
            {{ Form::label('login-email', 'Email', ['class' => 'control-label']) }}
            {{ Form::text('login[email]', '', ['class' => 'form-control', 'id' => 'login-email']) }}
        </div>
        <div class="form-group">
            {{ Form::label('login-password', 'Password', ['class' => 'control-label']) }}
            {{ Form::password('login[password]', ['class' => 'form-control', 'id' => 'login-password']) }}
        </div>
        {{ Form::submit('Log In', ['class' => 'btn btn-default btn-primary pull-right']) }}
    {{ Form::close() }}
@endif
