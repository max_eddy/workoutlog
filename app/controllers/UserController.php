<?php
namespace WL\Controllers;

use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use \Illuminate\Routing\UrlGenerator;
use Illuminate\Session\SessionManager;
use Illuminate\View\Environment;
use WL\Repositories\UserRepositoryInterface;

class UserController extends BaseController
{
    private $Auth;

    private $Input;

    private $Redirect;

    private $URL;

    private $Session;

    private $View;

    private $UserRepo;

    protected $layout = 'layout';

    public function __construct(
        AuthManager $auth,
        Request $input,
        Redirector $redirect,
        URLGenerator $url,
        SessionManager $session,
        Environment $view,
        UserRepositoryInterface $userRepo
    ) {
        $this->Auth = $auth;
        $this->Input = $input;
        $this->Redirect = $redirect;
        $this->URL = $url;
        $this->Session = $session;
        $this->View = $view;
        $this->UserRepo = $userRepo;
    }

    /**
     * Process a request to create a new user.
     *
     * @return Response
     */
    public function signUpUser()
    {
        if ($this->UserRepo->create($this->Input->get('signup', []))) {
            return $this->Redirect->route('showHome')->with('signUpSucceeded', true);
        }

        return $this->Redirect->route('showHome')->withErrors($this->UserRepo->getErrors())->withInput();
    }

    /**
     * Process a request to login as a user.
     *
     * @return Response
     */
    public function loginUser()
    {
        $input = $this->Input->get('login', []);
        if ($this->Auth->attempt($input)) {
            return $this->Redirect->intended($this->URL->route('showHome'));
        }

        return $this->Redirect->route('showHome')->with('loginFailed', true);
    }

    /**
     * Process a request to logout of a user session.
     *
     * @return Response
     */
    public function logoutUser()
    {
        $this->Auth->logout();

        return $this->Redirect->route('showHome');
    }

    /**
     * Display the profile page.
     *
     * @return Response
     */
    public function showProfilePage()
    {
        $this->layout->content = $this->View->make('user.profile', [
            'user' => $this->Auth->user(),
            'profileUpdateSucceeded' => $this->Session->get('profileUpdateSucceeded', false)
        ]);
    }

    /**
     * Process a request to update a user's information.
     *
     * @return Response
     */
    public function updateProfile()
    {
        $user = $this->Auth->user();
        if ($this->UserRepo->update($user->getKey(), $this->Input->all())) {
            return $this->Redirect->route('showProfile')->with('profileUpdateSucceeded', true);
        }

        return $this->Redirect->route('showProfile')->withErrors($this->UserRepo->getErrors())->withInput();
    }
}
