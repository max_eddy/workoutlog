<?php

Validator::extend('password', function ($attribute, $value, $parameters) use ($app) {
    return Hash::check($value, Auth::user()->getAuthPassword());
});

// Alpha, dash, space, underscore
Validator::extend('adsu', function ($attribute, $value, $parameters) {
    return (bool)preg_match('/^[\w\s-_]+$/', $value);
});
