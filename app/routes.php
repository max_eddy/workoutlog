<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Pages
Route::get('/', [
    'as'   => 'showHome',
    'uses' => 'WL\Controllers\PageController@showHomePage'
]);

// User Actions
Route::post('/signup', [
    'as'   => 'processSignUp',
    'uses' => 'WL\Controllers\UserController@signUpUser'
]);

Route::post('/login', [
    'as'   => 'processLogin',
    'uses' => 'WL\Controllers\UserController@loginUser'
]);

Route::post('/logout', [
    'as'     => 'processLogout',
    'before' => 'auth',
    'uses'   => 'WL\Controllers\UserController@logoutUser'
]);

Route::get('/profile', [
    'as'     => 'showProfile',
    'before' => 'auth',
    'uses'   => 'WL\Controllers\UserController@showProfilePage'
]);

Route::post('/profile', [
    'as'     => 'processProfile',
    'before' => 'auth',
    'uses'   => 'WL\Controllers\UserController@updateProfile'
]);

// Workout
Route::resource(
    'workout',
    'WL\Controllers\WorkoutController',
    [
        'before' => 'auth',
        'only' => ['index', 'store', 'show', 'update', 'destroy']
    ]
);
