<?php

namespace WL\Models;

class Session extends Model
{
    public function sets()
    {
        return $this->hasMany('WL\Models\Set');
    }
}
