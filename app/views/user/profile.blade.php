@section('content')
<h1>Profile</h1>
@if ($profileUpdateSucceeded)
    <p class="alert alert-success">Your profile was updated!</p>
@endif
{{ Form::model($user, ['route' => 'processProfile', 'method' => 'post', 'role' => 'form', 'id' => 'profile-update']) }}
    <div class="row form-group">
        <div class="col-md-6 {{ $errors->first('first_name') ? 'has-error' : '' }}">
            {{ Form::label('profile-first-name', 'First Name', ['class' => 'control-label']) }}
            {{ Form::text('first_name', null, ['class' => 'form-control', 'id' => 'profile-first-name']) }}
            <span class="help-block">{{ $errors->first('first_name') }}</span>
        </div>
        <div class="col-md-6 {{ $errors->first('last_name') ? 'has-error' : '' }}">
            {{ Form::label('profile-last-name', 'Last Name', ['class' => 'control-label']) }}
            {{ Form::text('last_name', null, ['class' => 'form-control', 'id' => 'profile-last-name']) }}
            <span class="help-block">{{ $errors->first('last_name') }}</span>
        </div>
    </div>
    <div class="form-group {{ $errors->first('email') ? 'has-error' : '' }}">
        {{ Form::label('profile-email', 'Email', ['class' => 'control-label']) }}
        {{ Form::text('email', null, ['class' => 'form-control', 'id' => 'profile-email']) }}
        <span class="help-block">{{ $errors->first('email') }}</span>
    </div>
    <h3>Change Password</h3>
    <div class="form-group {{ $errors->first('password') ? 'has-error' : '' }}">
        {{ Form::label('profile-password', 'Current Password', ['class' => 'control-label']) }}
        {{ Form::password('password', ['class' => 'form-control', 'id' => 'profile-password']) }}
        <span class="help-block">{{ $errors->first('password') }}</span>
    </div>
    <div class="form-group {{ $errors->first('new_password') ? 'has-error' : '' }}">
        {{ Form::label('profile-new-password', 'New Password', ['class' => 'control-label']) }}
        {{ Form::password('new_password', ['class' => 'form-control', 'id' => 'profile-password']) }}
        <span class="help-block">{{ $errors->first('new_password') }}</span>
    </div>
    <div class="form-group {{ $errors->first('new_password_confirmation') ? 'has-error' : '' }}">
        {{ Form::label('profile-new-confirmation', 'Re-enter New Password', ['class' => 'control-label']) }}
        {{ Form::password('new_password_confirmation', ['class' => 'form-control', 'id' => 'signup-password-confirmation']) }}
        <span class="help-block">{{ $errors->first('new_password_confriatmion') }}</span>
    </div>
    {{ Form::submit('Update Profile', ['class' => 'btn btn-default btn-primary pull-right']) }}
{{ Form::close() }}
@stop
