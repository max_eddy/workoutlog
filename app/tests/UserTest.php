<?php

namespace WL\Tests;

use Auth;
use WL\Models\User;

class UserTest extends TestCase
{
    protected $ValidInfo;

    public function setUp()
    {
        parent::setUp();
        $this->ValidInfo = [
            'first_name' => 'Max',
            'last_name' => 'Eddy',
            'email' => 'test@email.com',
            'password' => '123456',
            'password_confirmation' => '123456'
        ];
    }

    public function testValidCreateInputPasses()
    {
        $user = new User;
        $this->assertTrue($user->validate($this->ValidInfo));
    }

    public function testInvalidCreateInputFails()
    {
        $user = new User;
        $this->assertFalse($user->validate([
            'first_name' => 'Max',
            'last_name' => 'Eddy',
            'email' => 'test@email.com',
            'password' => '123456',
            'password_confirmation' => '1234567'
        ]));
    }

    public function testValidUpdateInputPasses()
    {
        $user = User::create($this->ValidInfo);
        Auth::shouldReceive('user')->andReturn($user);

        $this->assertTrue($user->validate([
            'first_name' => 'John',
            'last_name' => 'Smith',
            'email' => 'test2@email.com',
            'password' => '123456',
            'new_password' => '1234567',
            'new_password_confirmation' => '1234567'
        ]));
    }

    public function testInvalidUpdateInputFails()
    {
        $user = User::create($this->ValidInfo);
        Auth::shouldReceive('user')->andReturn($user);

        $this->assertFalse($user->validate([
            'first_name' => 'John',
            'last_name' => 'Smith',
            'email' => 'test2@email.com',
            'password' => '12345',
            'new_password' => '1234567',
            'new_password_confirmation' => '1234567'
        ]));
    }

    public function passwordIsHashedOnSave()
    {
        $user = User::create($this->ValidInfo);
        $this->assertFalse(Hash::needsRefresh($user->password));
    }
}
