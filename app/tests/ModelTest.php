<?php

namespace WL\Tests;

use Schema;
use Mockery;
use WL\Models\Model;

class ModelTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        // Create TestModel Table
        Schema::create('mock_models', function ($table) {
            $table->increments('id');
            $table->string('email');
            $table->string('name');
            $table->string('password');
            $table->timestamps();
        });
    }

    public function testValidModelCanSave()
    {
        $mock = new MockModel;
        $mock->name = 'Max Eddy';
        $mock->email = 'max@eddy.com';
        $mock->password = 'password1234';
        $this->assertTrue($mock->save());
    }

    public function testInvalidModelCannotSave()
    {
        $mock = new MockModel;
        $mock->name = 1234;
        $mock->email = '!@#$';
        $mock->password = null;

        $this->assertFalse($mock->save());
    }

    public function testErrorsPresentAfterFailedValidation()
    {
        $mock = new MockModel;
        $mock->save();

        $this->assertInstanceOf('Illuminate\Support\MessageBag', $mock->getErrors());
    }
}
