@if ($signUpSucceeded)
    <p class="alert alert-success">Your account was created!</p>
@endif
    {{ Form::open(['route' => 'processSignUp', 'method' => 'post', 'role' => 'form', 'id' => 'signup']) }}
        <div class="row form-group">
            <div class="col-md-6 {{ $errors->first('first_name') ? 'has-error' : '' }}">
                {{ Form::label('signup-first-name', 'First Name', ['class' => 'control-label']) }}
                {{ Form::text('signup[first_name]', '', ['class' => 'form-control', 'id' => 'signup-first-name']) }}
                <span class="help-block">{{ $errors->first('first_name') }}</span>
            </div>
            <div class="col-md-6 {{ $errors->first('last_name') ? 'has-error' : '' }}">
                {{ Form::label('signup-last-name', 'Last Name', ['class' => 'control-label']) }}
                {{ Form::text('signup[last_name]', '', ['class' => 'form-control', 'id' => 'signup-last-name']) }}
                <span class="help-block">{{ $errors->first('last_name') }}</span>
            </div>
        </div>
        <div class="form-group {{ $errors->first('email') ? 'has-error' : '' }}">
            {{ Form::label('signup-email', 'Email', ['class' => 'control-label']) }}
            {{ Form::text('signup[email]', '', ['class' => 'form-control', 'id' => 'signup-email']) }}
            <span class="help-block">{{ $errors->first('email') }}</span>
        </div>
        <div class="form-group {{ $errors->first('password') ? 'has-error' : '' }}">
            {{ Form::label('signup-password', 'Password', ['class' => 'control-label']) }}
            {{ Form::password('signup[password]', ['class' => 'form-control', 'id' => 'signup-password']) }}
            <span class="help-block">{{ $errors->first('password') }}</span>
        </div>
        <div class="form-group {{ $errors->first('password_confirmation') ? 'has-error' : '' }}">
            {{ Form::label('signup-password-confirmation', 'Re-enter Password', ['class' => 'control-label']) }}
            {{ Form::password('signup[password_confirmation]', ['class' => 'form-control', 'id' => 'signup-password-confirmation']) }}
            <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
        </div>
    {{ Form::submit('Sign Up', ['class' => 'btn btn-default btn-primary pull-right']) }}
{{ Form::close() }}
