<?php
namespace WL\Models;

use App;
use Illuminate\Validation\Factory;
use Illuminate\Database\Eloquent\Model as Eloquent;

abstract class Model extends Eloquent
{
    protected $Errors;

    protected $Validator;

    protected static $Rules = [];

    protected static $Messages = [];

    public function __construct(array $attributes = array(), Factory $validator = null)
    {
        parent::__construct($attributes);
        $this->Validator = $validator ?: App::make('validator');
    }

    public function isValid()
    {
        return $this->validate($this->attributes);
    }

    public function validate(array $data)
    {
        $validator = $this->getValidator($data);
        if ($validator->fails()) {
            $this->Errors = $validator->messages();
            return false;
        }

        return true;
    }

    public function getErrors()
    {
        return $this->Errors;
    }

    protected function getValidator(array $data)
    {
        return $this->Validator->make($data, static::$Rules, static::$Messages);
    }

    public static function boot()
    {
        parent::boot();

        static::saving(array(get_called_class(), 'checkBeforeSave'));
    }

    protected static function checkBeforeSave($model)
    {
        if (!$model->isValid()) {
            return false;
        }

        // Remove confirmation fields before save.
        foreach ($model->attributes as $name => $value) {
            if (strpos($name, 'confirmation') !== false) {
                unset($model->attributes[$name]);
            }
        }

        return true;
    }
}
