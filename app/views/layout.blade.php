<!DOCTYPE html>
<html>
<head>
    <title>Workout Log</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/" class="navbar-brand">Workout Log</a>
            </div>
            <div class="navbar-collapse collapse" id="nav-links">
                <ul class="nav navbar-nav">
                    <li class="{{ Route::currentRouteName() === 'showProfile' ? 'active' : '' }}"><a href="{{ route('showProfile') }}">Profile</a></li>
                    <li class="{{ Route::currentRouteName() === 'workout.index' ? 'active' : '' }}"><a href="{{ route('workout.index') }}">Workouts</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <section id="content" class="container">
    @yield('content')
    </section>

    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/angular.min.js"></script>
    <script src="js/angular.route.min.js"></script>
    <script src="app/app.js"></script>
    <script src="app/app.controllers.js"></script>
    <script src="app/app.workout-service.js"></script>
</body>
</html>
