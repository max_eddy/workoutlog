<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions', function (BluePrint $table) {
            $table->increments('id');
            $table->integer('workout_id')->unsigned();
            $table->timestamps();
            $table->foreign('workout_id')
                ->references('id')->on('workouts')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sessions', function (BluePrint $table) {
            $table->dropForeign('sessions_workout_id_foreign');
        });

        Schema::drop('sessions');
    }
}
