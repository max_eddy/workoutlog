<?php
App::bind('WL\Repositories\UserRepositoryInterface', 'WL\Repositories\EloquentUserRepository');

App::bind('WL\Repositories\WorkoutRepositoryInterface', 'WL\Repositories\EloquentWorkoutRepository');
