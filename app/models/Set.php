<?php

namespace WL\Models;

class Set extends Set
{
    protected $fillable = ['reps', 'weight'];

    protected static $Rules = [
        'reps' => '',
        'weight' => ''
    ];

    public function session()
    {
        return $this->belongsTo('WL\Model\Session');
    }

    public function exercise()
    {
        return $this->belongsto('WL\Model\Exercise');
    }
}
