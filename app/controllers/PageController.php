<?php
namespace WL\Controllers;

use Illuminate\Auth\AuthManager;
use Illuminate\Session\SessionManager;
use Illuminate\View\Environment;

class PageController extends BaseController
{
    private $Auth;

    private $Session;

    private $View;

    protected $layout = 'layout';

    public function __construct(AuthManager $auth, SessionManager $session, Environment $view)
    {
        $this->Auth = $auth;
        $this->Session = $session;
        $this->View = $view;
    }

    /**
     * Display the home page.
     *
     * @return Response
     */
    public function showHomePage()
    {
        $user = $this->Auth->user();
        $name = $user ? $user->first_name . ' ' . $user->last_name : '';
        $this->layout->content = $this->View->make('home', [
            'loggedIn'        => $this->Auth->check(),
            'name'            => $name,
            'signUpSucceeded' => $this->Session->get('signUpSucceeded', false),
            'loginFailed'     => $this->Session->get('loginFailed', false)
        ]);
    }
}
