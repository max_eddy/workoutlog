<?php

namespace WL\Models;

use App;
use Closure;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Hashing\HasherInterface;
use Illuminate\Validation\Factory;

class User extends Model implements UserInterface, RemindableInterface
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];

    protected $fillable = ['email', 'password', 'password_confirmation', 'first_name', 'last_name'];

    protected $Hash;

    protected static $Rules = [
        'first_name'                => 'required|alpha_num|min:2',
        'last_name'                 => 'required|alpha_num|min:2',
        'email'                     => 'required|email',
        'password'                  => 'min:6',
        'password_confirmation'     => 'min:6',
        'new_password'              => 'min:6|confirmed',
        'new_password_confirmation' => 'min:6'
    ];

    protected static $Messages = [
        'password' => 'Invalid Password'
    ];

    public function __construct(
        array $attributes = array(),
        Factory $validator = null,
        HasherInterface $hasher = null
    ) {
        parent::__construct($attributes, $validator);
        $this->Hash = $validator ?: App::make('hash');
    }

    public function workouts()
    {
        return $this->hasMany('WL\Models\Workouts');
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    protected function getValidator(array $data)
    {
        $validator = parent::getValidator($data);

        // Check email is unique if new user, or user is updating email.
        $validator->sometimes('email', 'unique:users,email', Closure::bind(function ($input) {
            return empty($this->email) || $this->email !== $input->email;
        }, $this));

        // Password required if new user.
        $validator->sometimes('password', 'required|confirmed', Closure::bind(function ($input) {
            return is_null($this->getKey());
        }, $this));

        // Check current password is correct if user attempts to update it.
        $validator->sometimes('password', 'required_with:new_password|password', Closure::bind(function ($input) {
            return $this->getKey() && $input->new_password;
        }, $this));

        return $validator;
    }

    protected static function checkBeforesave($model)
    {
        if (parent::checkBeforeSave($model)) {
            if ($model->Hash->needsRehash($model->password)) {
                $model->password = $model->Hash->make($model->password);
            }

            return true;
        }

        return false;
    }
}
