<?php

namespace WL\Tests;

use Artisan;
use Mockery;

class TestCase extends \Illuminate\Foundation\Testing\TestCase
{

    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate');
        $this->resetEvents();
    }

    /**
     * Creates the application.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernelInterface
     */
    public function createApplication()
    {
        $unitTesting = true;

        $testEnvironment = 'testing';

        return require __DIR__.'/../../bootstrap/start.php';
    }

    public function tearDown()
    {
        Mockery::close();
    }

    private function resetEvents()
    {
        // Define the models that have event listeners.
        $models = array('WL\Tests\MockModel', 'WL\Models\User');

        // Reset their event listeners.
        foreach ($models as $model) {

            // Flush any existing listeners.
            call_user_func(array($model, 'flushEventListeners'));

            // Reregister them.
            call_user_func(array($model, 'boot'));

            $user = new \WL\Models\User;
        }
    }
}
