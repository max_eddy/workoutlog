@section('content')
<div class="row">
    <div class="col-md-6">
        <h1>What is this?</h1>
        <p>Create workouts, log exercises, track progress etc..</p>
    </div>
    <div class="col-md-6">
        <h2>Login</h2>
        @include('user.login')
        @if (!$loggedIn)
            <h3 class="text-center">- OR -</h3>
            <h2>Sign Up</h2>
            @include('user.signup')
        @endif
    </div>
</div>
@stop
